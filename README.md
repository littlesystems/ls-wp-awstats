# README #

##LS Awstats 1.0##

[Awstats](http://www.awstats.org/) visitor statistics displayed on the Wordpress Dashboard.  Requires Awstats server configuration, optionally using Little System's proprietary [Application](https://bitbucket.org/littlesystems/application) scripts.

Copyright (c) 2017 [Little Systems](https://littlesystems.com.au).  All rights reserved.
