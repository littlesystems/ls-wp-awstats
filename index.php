<?php
namespace LittleSystems\WP\Plugin\LS_Awstats;
/*
Plugin Name: LS WP Awstats
Plugin URI: https://bitbucket.org/littlesystems/ls-wp-awstats
Description: <a href="http://www.awstats.org/">Awstats</a> visitor statistics displayed on the Wordpress Dashboard.  Requires Awstats server configuration, optionally using Little System's proprietary <a href="https://bitbucket.org/littlesystems/application">Application</a> scripts.
Author: Little Systems.
Version: 1.0 | Updated: December, 2016
Author URI: http://www.littlesystems.com.au
*/

// **********
// Awstats data call
// **********
function callServer($myOptions = '') {
    // require wordpress.ini
    $myIniFileName = '/etc/wordpress/wordpress.ini';
    if (!file_exists($myIniFileName)) {
        echo "$myIniFileName not found.";
        return false;
    }

    // determine awstats path
    $myIni          = parse_ini_file($myIniFileName, true);
    $myAwstatsPath  = $myIni['awstats'];
    $myDomain       = get_blog_details()->domain;
    $myUrl          = $myAwstatsPath."?config=$myDomain$myOptions&framename=mainright";

    // is self-signed?
    $myCert = $myIni['cert'];
    if ($myCert == 'self-signed') {
        $myContext = stream_context_create([
            "ssl" => [
                "verify_peer" => false,
                "allow_self_signed" => false,
            ]
        ]);
        $myStats = file_get_contents($myUrl, false, $myContext);
    } else {
      $myStats = file_get_contents($myUrl);
    }

    return $myStats;
}

// **********
// Dashboard Widget
// **********
add_action('wp_dashboard_setup', __NAMESPACE__.'\\add_dashboard_widget' );

function add_dashboard_widget() 
{
    wp_add_dashboard_widget(__NAMESPACE__.'dashboard_widget', 'Visitor Statistics', __NAMESPACE__.'\\dashboard_widget_content');
}

// Output the dashboard widget
function dashboard_widget_content() 
{
    global $blog_id;

    $myContents = get_option(__NAMESPACE__.'dashboard_widget_content');
    if (!$myContents) {
        $myContents = '&nbsp;&nbsp;Loading...';
    }

    print '<div id="awstats_dashboard_widget_content" onclick="location.href=\'./?page=visitor-statistics\';" style="cursor: pointer;">'.$myContents.'</div>';

    print '<p>&nbsp;&nbsp;See the full <a href="./?page=visitor-statistics">visitor statistics</a>.<p>';
}

// **********
// Dasbhoard Admin Menu
// **********
add_action('admin_menu', __NAMESPACE__.'\\dashboard_admin_menu');

function dashboard_admin_menu() 
{
    add_dashboard_page('Full Statistics by Awstats', 'Visitor Statistics', 'read', 'visitor-statistics', __NAMESPACE__.'\\dashboard_admin_menu_content');
}

function dashboard_admin_menu_content() 
{
    global $blog_id;

    echo '<div id="awstats" class="wrap">';
    echo '<h1>Visitor Statistics</h1>';

    #echo '<p>Back to Dashboard</p>';

    $myOptionalsString = '';
    $myOptionals = array('month','year','output','hostfilter','hostfilterex','urlfilter','urlfilterex','refererpagesfilter','refererpagesfilterex');
    foreach ($myOptionals as $myOptional) {
      if (isset($_REQUEST[$myOptional])) {
        $myOptionalsString.= "&amp;$myOptional={$_REQUEST[$myOptional]}";
      }
    }

    $myAnchor = array( 
        "alldomains"      => "countries",
        "unknownip"       => "visitors",
        "lasthosts"       => "visitors",
        "allhosts"        => "visitors",
        "lastrobots"      => "robots",
        "allrobots"       => "robots",
        "downloads"       => "downloads",
        "urlexit"         => "exit",
        "urlentry"        => "entry",
        "urldetail"       => "urls",
        "unknownos"       => "os",
        "osdetail"        => "os",
        "unknownbrowser"  => "browsers",
        "browserdetail"   => "browsers",
        "refererse"       => "referer",
        "refererpages"    => "refering",
        "keyphrases"      => "keyphrases",
        "keywords"        => "keywords",
        "errors404"       => "errors",
    );
    $myAnchorString = isset($_REQUEST['output']) ? '#'.$myAnchor[$_REQUEST['output']] : '';

    $myStats = callServer($myOptionalsString);

    $myStats = substr($myStats, strpos($myStats, '<a name="top"></a>'));
    $myStats = substr($myStats, 0, strpos($myStats, '<br />
</body>
</html>'));

    $myStats = str_replace('</form><br />

<table class="aws_border sortable" border="0" cellpadding="2" cellspacing="0" width="100%">
<tr><td class="aws_title" width="70%">Summary', '</form><br />

<table>
<tr><td class="aws"><a href="./">Back to Dashboard</a></td></tr>
</table>
<br />

<table class="aws_border sortable" border="0" cellpadding="2" cellspacing="0" width="100%">
<tr><td class="aws_title" width="70%">Summary', $myStats);

    if (isset($_REQUEST['output'])) {
        $myPostString = '&output='.$_REQUEST['output'];
    } else {
        $myPostString = '';
    }
  
    $myStats = str_replace('src="/icon','src="/wp-admin/images/icon', $myStats);
    $myStats = preg_replace('/action="awstats\.pl\?.*"/', 'action="./?page=visitor-statistics'.$myPostString.'" method="post"', $myStats);
    $myStats = preg_replace('/<a.*>Back to main page<\/a>/', '<a href="./?page=visitor-statistics'.$myAnchorString.'">Back to main page</a>', $myStats);
    $myStats = str_replace('href="awstats.pl?','href="./?page=visitor-statistics&amp;', $myStats);
    $myStats = str_replace(' target="_parent"',' method="post"', $myStats);
    $myStats = str_replace('" class="aws_border"','" method="post"', $myStats);
    $myStats = str_replace(' target="mainright"','',$myStats);
    $myStats = str_replace('&amp;framename=mainright','',$myStats);
    echo $myStats;
    echo "</div>";
}

function javascript() 
{ 
?>
<script type="text/javascript" >
jQuery(document).ready(function($) {

    var data = {
        'action': 'awstats-dashboard',
    };

    // since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
    jQuery.post(ajaxurl, data, function(response) {
          jQuery("div#awstats_dashboard_widget_content").html(response);
    });
});
</script> 
<?php
}
add_action( 'admin_footer', __NAMESPACE__.'\\javascript' );

function dashboard_callback() 
{
    global $wpdb; // this is how you get access to the database

    $myStats = callServer();

    $myStart = '<tr><td class="aws_title" width="70%">Monthly history </td><td class="aws_blank">&nbsp;</td></tr>
<tr><td colspan="2">
<table class="aws_data" border="1" cellpadding="2" cellspacing="0" width="100%">
<tr><td align="center">
<center>';
    $myStrpos = strpos($myStats, $myStart);
    $myStats = substr($myStats, $myStrpos + strlen($myStart));
    $myStats = substr($myStats, 0, strpos($myStats, "</table>")+8);

    $myStats = str_replace('src="/icon','src="/wp-admin/images/icon', $myStats);
    $myStats = str_replace('<br />2016','', $myStats);
    $myStats = str_replace('width="6"','width="20%" min-width="3"', $myStats);

    echo $myStats;
    update_option(__NAMESPACE__.'dashboard_widget_content', $myStats);

    wp_die(); // this is required to terminate immediately and return a proper response
}
add_action( 'wp_ajax_awstats-dashboard', __NAMESPACE__.'\\dashboard_callback' );


// enqueue CSS files
function admin_enqueue_scripts($hook) 
{
    if ($hook=="index.php") {
        wp_enqueue_style( __NAMESPACE__.'dashboard_custom_css', plugin_dir_url( __FILE__ ) . 'css/dashboard.css' );
    } elseif ($hook=="dashboard_page_visitor-statistics") {
        wp_enqueue_style( __NAMESPACE__.'dashboard_page_custom_css', plugin_dir_url( __FILE__ ) . 'css/dashboard-page.css' );
    }
}
add_action( 'admin_enqueue_scripts', __NAMESPACE__.'\\admin_enqueue_scripts' );